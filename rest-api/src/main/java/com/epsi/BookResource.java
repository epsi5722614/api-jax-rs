package com.epsi;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/books")
public class BookResource {
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String getBooks() {
    return "GET";
  }

  @POST
  @Produces(MediaType.TEXT_PLAIN)
  public String createBooks() {
    return "POST";
  }

  @PUT
  @Produces(MediaType.TEXT_PLAIN)
  public String updateBooks() {
    return "PUT";
  }

  @DELETE
  @Produces(MediaType.TEXT_PLAIN)
  public String deleteBooks() {
    return "DELETE";
  }
}
